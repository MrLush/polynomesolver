import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.solvers.BrentSolver;


import static java.lang.Math.round;

public class BrentMethod{
    private double a = 15, b = -221, c = -48, d = 55, e = 188;
    private double[] x = new double[4];
    private double intervalStart = -10000;
    private double intervalStop = 10000;
    private double intervalSize = 0.001;

    public double[] solveIt () {
        GUI gui = GUI.getInstance();
        double intStart = intervalStart;
        BrentSolver solver = new BrentSolver();
        UnivariateFunction function = x -> a*Math.pow(x, 4.0) + b*Math.pow(x, 3.0) + c*Math.pow(x, 2.0) + d*x + e;
        int i = 0;
        int percent = 1;
        while (intStart < intervalStop) {
            intStart += intervalSize;

            double iter = intStart / intervalSize;
            int result = (int)Math.round(iter);
            iter = result * intervalSize;

            if(iter % ((Math.abs(intervalStart)+Math.abs(intervalStop))/100) == 0){
                System.out.println("Iter - " + iter);
                System.out.println("Progress - " + percent + "%");
                gui.setjProgressBar1(percent);
                percent++;
            }

            if(Math.signum(function.value(intStart)) != Math.signum(function.value(intStart+intervalSize))) {
                x[i]=solver.solve(1000, function, intStart, intStart+intervalSize);
                System.out.println("Solver say that x(" + i + ") = " + x[i]);
                i++;
            }
        }
        return x;
    }
    public double getA() {
            return a;
        }

    public void setA(double a) {
            this.a = a;
        }

    public double getB() {
            return b;
        }

    public void setB(double b) {
            this.b = b;
        }

    public double getC() {
            return c;
        }

    public void setC(double c) {
            this.c = c;
        }

    public double getD() {
            return d;
        }

    public void setD(double d) {
            this.d = d;
        }

    public double getE() {
            return e;
        }

    public void setE(double e) {
            this.e = e;
        }

    public void setIntervalStart(double intervalStart) {
            this.intervalStart = intervalStart;
        }

    public void setIntervalStop(double intervalStop) {
            this.intervalStop = intervalStop;
        }

    public void setIntervalSize(double intervalSize) {
        this.intervalSize = intervalSize;
    }

    public double getIntervalStart() {
        return intervalStart;
    }

    public double getIntervalStop() {
        return intervalStop;
    }
}