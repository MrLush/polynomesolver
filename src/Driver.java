import javax.swing.*;

/**
 * @author Lushnikov Daniil M621 20.04.2020
 */

public class Driver{
    public static void main(String[] args) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        // Create and display the form
        GUI gui = GUI.getInstance();
        java.awt.EventQueue.invokeLater(() -> gui.setVisible(true));
    }
}