PolynomeSolver

This application is designed to calculate equations. Increased the speed of calculations and simplify calculations for people, so as not to make lengthy calculations manually.

Interface:

![Interface](/img/Screenshot_1.png "Interface")

Test 1:

![Test](/img/Screenshot_2.png "Test")

Test 2:

![Test 2](/img/Screenshot_3.png "Test 2")